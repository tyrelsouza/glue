import unittest
from glue import glue


class GlueTests(unittest.TestCase):

      def test_tuple(self):
          output = glue("why", "would", "you", "run", "these", "tests")
          assert output == "why would you run these tests"

      def test_tuple_custom_separator(self):
          output = glue("why", "would", "you", separator=",")
          assert output == "why,would,you"

      def test_tuple_and_named(self):
          output = glue("why", "would", "you", "run", "these", "tests", who="bozo")
          assert output == "why would you run these tests bozo"
          assert "who" not in output

      def test_named(self):
          output = glue(what="bozo", who="clown")
          assert output == "bozo clown"
          assert "what" not in output

      def test_flaten(self):
          output = glue("why", ["would", ["you", "run"], "these"], "tests")
          assert output == "why would you run these tests"

      def test_objects(self):
          output = glue("why", ["would", ["you", "run"], "these"], "tests", 12345)
          assert output == "why would you run these tests 12345"
          output = glue("why", ["would", ["you", "run"], "these"], "tests", False)
          assert output == "why would you run these tests False"

if __name__ == '__main__':
    unittest.main()
